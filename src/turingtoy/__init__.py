from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    state = machine['start state']
    tape = list(input_)
    head = 0
    step_count = 0
    final_states = machine['final states']
    execution_history = []

    while True:
        if state in final_states:
            tape = remove_blanks(tape, machine['blank'])
            return ''.join(tape), [machine, input_, ''.join(tape), execution_history], True

        print("Rapport ", step_count, ": ", ''.join(tape), " state = ", state)

        state, tape, head, execution_history = step_turing_machine(machine, state, tape, head, execution_history)
        step_count += 1

        if step_count > 400:
            print("plus de 400 steps, fin du programme")
            tape = remove_blanks(tape, machine['blank'])
            return ''.join(tape), [machine, input_,''.join(tape), execution_history], False

        if steps is not None and step_count >= steps:
            tape = remove_blanks(tape, machine['blank'])
            return ''.join(tape), [machine, input_,''.join(tape), execution_history], False


def step_turing_machine(machine, state, tape, head, execution_history):
    
    current_char = tape[head]
    print("current_char = ", current_char)

    rule = machine['table'][state].get(current_char)
    print("rule = ", rule)
    if not rule:
        raise Exception(f"No rule defined for state '{state}' and character '{current_char}'")

    execution_history.append({'state': state, 'reading': current_char, 'position': head, 'memory': ''.join(tape), 'transition': rule})
    
    if 'write' in rule:
        tape[head] = rule.get('write', current_char)

    if 'L' in rule:
        head -= 1
        if head == -1:
            tape.insert(0, machine['blank'])  
            head = 0
        if isinstance(rule, dict):
            state = rule.get('L', state)
    elif 'R' in rule:
        head += 1
        if head == len(tape):
            tape.append(machine['blank'])
        print("state just before crash = ", state)
        print("rule just before crash = ", rule)
        if isinstance(rule, dict):
            state = rule.get('R', state)

    return state, tape, head, execution_history #Resultat

def remove_blanks(tape: List[str], blank: str) -> List[str]:
    tape_str = ''.join(tape)
    tape_str = tape_str.lstrip(blank).rstrip(blank)
    return list(tape_str)
